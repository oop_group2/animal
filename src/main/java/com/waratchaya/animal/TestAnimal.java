/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.animal;

/**
 *
 * @author Melon
 */
public class TestAnimal {
    public static void main(String[] args) {
       Human h1 = new Human("Dang");
       h1.eat();
       h1.walk();
       h1.run();
       h1.speak();
       h1.sleep();
       System.out.println("h1 is animal ? " + (h1 instanceof Animal));
       System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));
       Animal a1 = h1;
       System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
       System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
       
       Cat c1 = new Cat("Lucky");
       c1.eat();
       c1.walk();
       c1.run();
       c1.speak();
       c1.sleep();
       System.out.println("c1 is animal ? " + (c1 instanceof Animal));
       System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));
       Animal a2 = c1;
       System.out.println("a1 is land animal ? " + (c1 instanceof LandAnimal));
       System.out.println("a1 is reptile animal ? " + (c1 instanceof Reptile));
       
       
       Dog d1 = new Dog ("Pony");
       d1.eat();
       d1.walk();
       d1.run();
       d1.speak();
       d1.sleep();
       System.out.println("d1 is animal ? " + (d1 instanceof Animal));
       System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));
       Animal a3 = d1;
       System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));
       System.out.println("d1 is reptile animal ? " + (d1 instanceof Reptile));
       
       Crocodile cro1 = new Crocodile("Milk");
       cro1.crawl();
       cro1.eat();
       cro1.walk();
       cro1.speak();
       cro1.sleep();
       System.out.println("cro1 is animal ? " + (cro1 instanceof Animal));
       System.out.println("cro1 is Reptile animal ? " + (cro1 instanceof Reptile));
       Animal a4 = cro1;
       System.out.println("cro1 is land animal ? " + (cro1 instanceof LandAnimal));
       System.out.println("cro1 is Aquatic animal ? " + (cro1 instanceof AquaticAnimal));
    
       Snake s1 = new Snake("Moji");
       s1.crawl();
       s1.eat();
       s1.walk();
       s1.speak();
       s1.sleep();
       System.out.println("s1 is animal ? " + (s1 instanceof Animal));
       System.out.println("s1 is Reptile animal ? " + (s1 instanceof Reptile));
       Animal a5 = s1;
       System.out.println("s1 is land animal ? " + (s1 instanceof LandAnimal));
       System.out.println("s1 is Aquatic animal ? " + (s1 instanceof AquaticAnimal));
       
       Fish f1 = new Fish("Cake");
       f1.swim();
       f1.eat();
       f1.walk();
       f1.speak();
       f1.sleep();
       System.out.println("f1 is animal ? " + (f1 instanceof Animal));
       System.out.println("f1 is Aquatic animal ? " + (f1 instanceof AquaticAnimal));
       Animal a6 = f1;
       System.out.println("f1 is land animal ? " + (f1 instanceof LandAnimal));
       System.out.println("f1 is reptile animal ? " + (f1 instanceof Reptile));
       
       Crab cr1 = new Crab("Dango");
       cr1.swim();
       cr1.eat();
       cr1.walk();
       cr1.speak();
       cr1.sleep();
       System.out.println("cr1 is animal ? " + (cr1 instanceof Animal));
       System.out.println("cr1 is Aquatic animal ? " + (cr1 instanceof AquaticAnimal));
       Animal a7 = cr1;
       System.out.println("cr1 is land animal ? " + (cr1 instanceof LandAnimal));
       System.out.println("cr1 is reptile animal ? " + (cr1 instanceof Reptile));
       
       Bat b1 = new Bat("Daifuku");
       b1.fly();
       b1.eat();
       b1.walk();
       b1.speak();
       b1.sleep();
       System.out.println("b1 is animal ? " + (b1 instanceof Animal));
       System.out.println("b1 is Poultry animal ? " + (b1 instanceof Poultry));
       Animal a8 = b1;
       System.out.println("b1 is land animal ? " + (b1 instanceof LandAnimal));
       System.out.println("b1 is reptile animal ? " + (b1 instanceof Reptile));
       
       Bird bi1 = new Bird("Melonpan");
       bi1.fly();
       bi1.eat();
       bi1.walk();
       bi1.speak();
       bi1.sleep();
       System.out.println("bi1 is animal ? " + (bi1 instanceof Animal));
       System.out.println("bi1 is Poultry animal ? " + (bi1 instanceof Poultry));
       Animal a9 = bi1;
       System.out.println("bi1 is land animal ? " + (bi1 instanceof LandAnimal));
       System.out.println("bi1 is reptile animal ? " + (bi1 instanceof Reptile));
    }
}
